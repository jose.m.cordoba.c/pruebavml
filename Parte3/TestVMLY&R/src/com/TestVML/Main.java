package com.TestVML;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Bienvenido, este programa le dice Cuales vocales están presentes en un texto, para continuar" +
                " ingrese su texto seguido de intro");
        Scanner s = new Scanner(System.in);
        String texto = s.nextLine();
        char[] vocales = Validator.obtenerVocalesOrdenadas(texto);
        System.out.println("En el texto: ");
        System.out.println(texto);
        System.out.println("Están presentes las siguientes vocales: " + Arrays.toString(vocales));
    }
}
