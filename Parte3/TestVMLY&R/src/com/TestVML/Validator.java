package com.TestVML;

import java.util.ArrayList;

public class Validator {

    private static final char[] vocales  = {'a', 'e', 'i', 'o', 'u'};

    /**
     * Obtiene las vocales Ordenadas en un texto
     *
     * @param texto Cadena de texto de Tamaño indefinida
     * @return Arreglo con las vocales presentes en el texto Ordenadas
     */
    static char[] obtenerVocalesOrdenadas(String texto){

        ArrayList<Character> resultadoInicial = new ArrayList<Character>();

        // Validamos las vocales
        for (char vocal:
             vocales) {
            if (texto.contains(String.valueOf(vocal))){
                resultadoInicial.add(vocal);
            }
        }

        // Cast a tipo char[]
        char[] resultado = new char[resultadoInicial.size()];
        for (int i = 0; i < resultadoInicial.size(); i++) {
            resultado[i] = resultadoInicial.get(i);
        }

        return resultado;
    }
}
