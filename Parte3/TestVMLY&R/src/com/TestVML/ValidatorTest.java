package com.TestVML;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {

    @org.junit.jupiter.api.Test
    void obtenerVocalesOrdenadas() {
        String dado = "eeeeaaaaoooouuuu";
        char[] espero = {'a', 'e', 'o', 'u'};
        char[] obtengo = Validator.obtenerVocalesOrdenadas(dado);
        assertArrayEquals(espero, obtengo);
    }
}
