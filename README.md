# PruebaVML

## Parte 1. 

Descargar los archivos adjuntos al correo, en ellos encontrara dos PSD (compare-desktop y
compare-mobile), usted debe de generar el markup necesario para acercar la experiencia web a los
diseños entregados, teniendo en cuenta lo siguiente:
* El HTML & CSS final debe ser responsive, soportando tres breakpoints primarios: mobile,
tablet y desktop.
* Puede usar CSS/LESS/SASS lo que considere es una buena práctica en el FE.
* NO puede usar JQuery, se aconseja el manejo de Vanilla Javascript de ser necesario.

## Parte 2. 

Usando el markup generado en la parte 1, cree un modelo en Javascript que tenga relación
a la estructura de datos presentada en el modal, ejemplo: un array de objetos que contenga cada
uno de los valores mostrados en pantalla. Al momento del usuario dar click en “See details”, se debe
generar un console.log del objeto seleccionado.

## Parte 3. 

Usando Java, implemente el código necesario para mostrar un arreglo con las vocales
ORDENADAS usadas un string con tamaño indefinido según una entrada por consola (System.in).
Siéntase libre de crear clases y modelos necesarios para ello.
### Ejemplo:
    entrada: "Hola! buena suerte!"
    salida: [a,e,o,u]